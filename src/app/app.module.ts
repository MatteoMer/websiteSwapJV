import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { PreferencesComponent } from './preferences/preferences.component';
import { AboutComponent } from './about/about.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';
import { MessagesComponent } from './messages/messages.component';
import { HomeComponent } from './home/home.component';
import { AuthServices } from './services/auth.services';
import { PostService } from './services/post.service';
import { HttpClientModule } from '@angular/common/http';
import { ListPostComponent } from './list-post/list-post.component';
import { PostComponent } from './post/post.component';

const appRoutes: Routes = [
	{ path: 'home', component: HomeComponent },
	{ path: 'register', component: RegisterComponent },
	{ path: 'login', component: LoginComponent },
	{ path: 'messages', component: MessagesComponent },
	{ path: 'preferences', component: PreferencesComponent },
	{ path: 'about', component: AboutComponent },
	{ path: '', component: HomeComponent }
];

@NgModule({
	declarations: [
		AppComponent,
		NavbarComponent,
		PreferencesComponent,
		AboutComponent,
		LoginComponent,
		RegisterComponent,
		MessagesComponent,
		HomeComponent,
		ListPostComponent,
		PostComponent
	],
	imports: [
		BrowserModule,
		RouterModule.forRoot(appRoutes),
		FormsModule,
		HttpClientModule
	],
	providers: [
		AuthServices,
		PostService
	],
	bootstrap: [AppComponent]
})
export class AppModule { }
