import { Component, OnInit } from '@angular/core';
import { AuthServices } from '../services/auth.services';

@Component({
	selector: 'app-home',
	templateUrl: './home.component.html',
	styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

	pseudo: string = undefined;
	constructor(private authService: AuthServices) { }

	ngOnInit() {
		this.authService.pseudoSubject.subscribe(
			(pseudo: string) => {
				this.pseudo = pseudo;
			}
		);
		this.authService.emitPseudoSubject();
	}

}
