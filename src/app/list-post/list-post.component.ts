import { Component, OnInit } from '@angular/core';
import { PostService } from '../services/post.service';

@Component({
	selector: 'app-list-post',
	templateUrl: './list-post.component.html',
	styleUrls: ['./list-post.component.css']
})
export class ListPostComponent implements OnInit {
	posts: any[];
	constructor(private postService: PostService) { }

	ngOnInit() {
		this.postService.postListSubject.subscribe(
			(list: any[]) => {
				this.posts = list;
			}
		);
		this.postService.getPostList();
	}

}
