import { Component, OnInit } from '@angular/core';
import { AuthServices } from '../services/auth.services';

@Component({
	selector: 'app-navbar',
	templateUrl: './navbar.component.html',
	styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {

	isAuth: boolean;
	constructor(private authService: AuthServices) { }

	ngOnInit() {
		this.authService.isAuthSubject.subscribe(
			(isAuth: boolean) => {
				this.isAuth = isAuth;
			}
		);
		this.authService.emitIsAuthSubject();
	}
}
