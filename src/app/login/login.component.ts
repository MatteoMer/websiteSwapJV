import { Component, OnInit } from '@angular/core';
import { AuthServices } from '../services/auth.services';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import md5 from 'blueimp-md5';

@Component({
	selector: 'app-login',
	templateUrl: './login.component.html',
	styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

	userlist: any[];
	constructor(private authService: AuthServices, private router: Router) { }

	checkIfUserAndPass(pseudo: string, pass: string): boolean {
		this.authService.getUserList();
		for (let i = 0; i < this.userlist.length; i++) {
			if (this.userlist[i]['pseudo'] === pseudo && this.userlist[i]['password'] === pass)
				return true;
		}
		console.log('here');
		return false;
	}

	onSubmit(form: NgForm) {
		let user = form.value['pseudoInput'];
		let pass = form.value['passwordInput'];
		if (this.checkIfUserAndPass(user, md5(pass)) === true) {
			this.authService.connectUser(user);
			this.router.navigate(['home']);
		}
		else {
			form.reset();
		}
	}

	ngOnInit() {
		this.authService.userListSubject.subscribe(
			(list: any[]) => {
				this.userlist = list;
			}
		);
		this.authService.emitUserListSubject();
		this.authService.getUserList();

	}

}
