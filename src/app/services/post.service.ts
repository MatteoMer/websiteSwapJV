import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class PostService {

	postListSubject = new Subject<any[]>();
	private postList: any[] = [];

	constructor(private httpClient: HttpClient) { }

	emitPostListSubject() {
		this.postListSubject.next(this.postList.slice());
	}

	getPostList() {
		this.httpClient
			.get<any[]>('http://localhost:3000/api/postlist')
			.subscribe(
				(res) => {
					this.postList = res;
					this.emitPostListSubject();
				},
				(err) => {
					console.log('error while asking API');
				}
			);
	}
}
