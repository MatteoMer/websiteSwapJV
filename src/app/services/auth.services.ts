import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';

@Injectable()
export class AuthServices {

	userListSubject = new Subject<any[]>();
	isAuthSubject = new Subject<boolean>();
	pseudoSubject = new Subject<string>();
	private userList: any[] = [];
	private isAuth: boolean = false;
	private pseudo: string;

	constructor(private httpClient: HttpClient) { }

	emitPseudoSubject(){
		this.pseudoSubject.next(this.pseudo);
	}

	emitIsAuthSubject(){
		this.isAuthSubject.next(this.isAuth);
	}

	emitUserListSubject() {
		this.userListSubject.next(this.userList.slice());
	}

	getUserList() {
		this.httpClient
			.get<any[]>('http://localhost:3000/api/userlist')
			.subscribe(
				(res) => {
					this.userList = res;
					this.emitUserListSubject();
				},
				(err) => {
					console.error('error');
				}
			);
	}

	addUser(pseudo: string, password: string) {
		this.getUserList();
		const userObject = {
			pseudo: '',
			password: ''
		};
		userObject.pseudo = pseudo;
		userObject.password = password;
		this.httpClient
			.post('http://localhost:3000/api/userlist', userObject)
			.subscribe(
				(data) => {
					this.isAuth = true;
					this.emitIsAuthSubject();
					this.pseudo = pseudo;
					this.emitPseudoSubject();
				},
				(err) => {
					console.log('error' + err);
				}
			);
	}

	connectUser(pseudo: string) {
		this.pseudo = pseudo;
		this.emitPseudoSubject();
		this.isAuth = true;
		this.emitIsAuthSubject();
	}
	
}
