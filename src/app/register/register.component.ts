import { Component, OnInit } from '@angular/core';
import { AuthServices } from '../services/auth.services';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import md5 from 'blueimp-md5';

@Component({
	selector: 'app-register',
	templateUrl: './register.component.html',
	styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

	userlist: any[];
	constructor(private authService: AuthServices, private router: Router) { }

	checkIfUserExist(pseudo: string): boolean {
		this.authService.getUserList();
		for (let i = 0; i < this.userlist.length; i++) {
			if (this.userlist[i]['pseudo'] === pseudo)
				return true;
		}
		console.log('here');
		return false;
	}

	onSubmit(form: NgForm) {
		let user = form.value['pseudoInput'];
		let pass = md5(form.value['passwordInput']);
		if (this.checkIfUserExist(user) === false)
		{
			this.authService.addUser(user,pass);
			this.router.navigate(['home']);
		}
		else {
			form.reset();
		}
	}

	ngOnInit() {
		this.authService.userListSubject.subscribe(
			(list: any[]) => {
				this.userlist = list;
			}
		);
		this.authService.emitUserListSubject();
		this.authService.getUserList();
	}

}
