const userListController = require('../controller/userList')
const postListController = require('../controller/postList')

module.exports = (app) => {
	app.route('/api/userlist').get(userListController.getAll);
	app.route('/api/userlist').post(userListController.create);
	app.route('/api/userlist/:id').get(userListController.get);
	app.route('/api/userlist/:id').put(userListController.update);
	app.route('/api/userlist/:id').delete(userListController.delete);
	app.route('/api/postlist').get(postListController.getAll);
	app.route('/api/postlist').post(postListController.create);
	app.route('/api/postlist/:id').get(postListController.get);
	app.route('/api/postlist/:id').put(postListController.update);
	app.route('/api/postlist/:id').delete(postListController.delete);

};
