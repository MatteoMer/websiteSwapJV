const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');
const port = process.env.PORT || '3000';
const app = express();
const routes = require('./api/route/index');
const mongoose = require('mongoose');

mongoose.Promise = global.Promise;
mongoose.connect('mongodb://admin:GameJamaicaHundred0RagJerome6@ds129153.mlab.com:29153/websitejv');

app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
app.use(function (req, res, next) {  // ENLEVER
	res.header("Access-Control-Allow-Origin", "*");
	res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
	next();
});
routes(app)

app.use(express.static(path.join(__dirname, '/dist/websiteJV')));

app.get('*', function (req, res) {
	res.sendFile(path.join(__dirname, 'dist/websiteJV/index.html'));
});

app.set('port', port);

const server = http.createServer(app);

server.listen(port, function () { console.log(`Running on port:${port}`)});
