const express = require('express');
const router = express.Router();

const axios = require('axios');
const API = 'https://jsonplaceholder.typicode.com';

router.get('/', function (req, res) {
	res.send('it works!');
});

router.get('/posts', function (req, res) {
	axios.get(`${API}/posts`)
	.then(posts => {
		res.status(200).json(posts.data);
	})
	.catch(err => {
		res.status(500).send(error);
	});
});

module.exports = router;
